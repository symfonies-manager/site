export interface SymfonyResponse {
    tld: string;
    port: number;
    domains: object;
    statuses: object;
}
