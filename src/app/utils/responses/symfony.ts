import {Command} from './command';

export interface Symfony {
    name: string;
    path: string;
    active: boolean;
    loading: boolean;
    favourite: boolean;
    url: string;
    entryPoints: string[];
    commands: Command[];
}
