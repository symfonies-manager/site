export interface Command {
    command: string;
    label: string;
    priority: number;
}
