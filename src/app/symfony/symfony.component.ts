import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppComponent} from '../app.component';
import {Symfony} from '../utils/responses/symfony';
import Swal from 'sweetalert2';
import {Command} from '../utils/responses/command';

@Component({
    // tslint:disable-next-line:component-selector
    selector: '[app-symfony]',
    templateUrl: './symfony.component.html',
    styleUrls: ['./symfony.component.scss']
})
export class SymfonyComponent implements OnInit {

    @Input() symfony: Symfony;
    @Input() proxyConfigurations;

    constructor(private httpClient: HttpClient, private appComponent: AppComponent) {
    }

    ngOnInit() {
        this.loadSymfony();
    }

    doStart() {
        if (this.symfony.loading) {
            return;
        }

        this.symfony.loading = true;

        this.symfony.commands.filter(command => command.priority < 0).sort((c1, c2) => {
            if (c1.priority < c2.priority) {
                return -1;
            }

            if (c1.priority > c2.priority) {
                return 1;
            }

            return 0;
        }).forEach(command => {
            this.runCommand(command);
        });

        this.httpClient.get('/api/symfony-proxies/start/' + this.symfony.name).subscribe(response => {
            this.symfony.commands.filter(command => command.priority > 0).sort((c1, c2) => {
                if (c1.priority < c2.priority) {
                    return 1;
                }

                if (c1.priority > c2.priority) {
                    return -1;
                }

                return 0;
            }).forEach(command => {
                this.runCommand(command);
            });

            this.appComponent.populateSymfonies(response);

            this.symfony.loading = false;
        });
    }

    doStop() {
        if (this.symfony.loading) {
            return;
        }

        this.symfony.loading = true;

        this.httpClient.get('/api/symfony-proxies/stop/' + this.symfony.name).subscribe(response => {
            this.appComponent.populateSymfonies(response);

            this.symfony.loading = false;
        });
    }

    hasLinks() {
        return this.symfony.entryPoints.length > 0;
    }

    addEntryPoint() {
        Swal.fire({
            title: 'Add new entry point',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Insert',
            showClass: {
                popup: 'animated fadeInDown faster'
            },
            hideClass: {
                popup: 'animated fadeOutUp faster'
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
            if (result.value) {
                this.symfony.entryPoints.push(result.value);

                this.saveEntryPoints();
            }
        });
    }

    removeEntryPoint(entryPoint) {
        this.symfony.entryPoints.splice(this.symfony.entryPoints.indexOf(entryPoint), 1);
        this.saveEntryPoints();
    }

    saveEntryPoints() {
        localStorage.setItem(this.symfony.name + '_ENTRYPOINTS', JSON.stringify(this.symfony.entryPoints));
    }

    addCommand() {
        Swal.mixin({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3'],
            showClass: {
                popup: 'animated fadeInDown faster'
            },
            hideClass: {
                popup: 'animated fadeOutUp faster'
            }
        }).queue([
            {
                title: 'Command',
                text: 'Type the command to launch on symfony'
            },
            {
                title: 'Label',
                text: 'Type the command label if you want'
            },
            // @ts-ignore
            {
                title: 'Start priority',
                html: `Select the priority for command on start
                    <div class="text-left">
                        <ul>
                            <li><small><b>0</b>: off</small></li>
                            <li><small><b>-10</b>: highest executed before the start</small></li>
                            <li><small><b>-1</b>: lowest executed before the start</small></li>
                            <li><small><b>1</b>: highest executed after the start</small></li>
                            <li><small><b>10</b>: lowest executed after the start</small></li>
                        </ul>
                    </div>
                `,
                input: 'range',
                inputValue: 0,
                inputAttributes: {
                    min: -10,
                    max: 10,
                    step: 1
                }
            }
        ]).then((result) => {
            if (result.value) {
                const command: Command = {
                    command: result.value[0],
                    label: result.value[1],
                    priority: result.value[2]
                };

                this.symfony.commands.push(command);

                this.saveCommands();
            }
        });
    }

    removeCommand(command: Command) {
        this.symfony.commands.splice(this.symfony.commands.indexOf(command), 1);
        this.saveCommands();
    }

    saveCommands() {
        localStorage.setItem(this.symfony.name + '_COMMANDS', JSON.stringify(this.symfony.commands));
    }

    executeCommand(command: Command) {
        Swal.fire({
            title: 'Execute command?',
            text: command.command,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Run',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                this.runCommand(command).subscribe(response => {
                    if (response) {
                        Swal.fire({
                            title: '<pre>' + command.command + '</pre>',
                            // icon: 'info',
                            html: '<pre class="text-left">' + response + '</pre>',
                            showCloseButton: true,
                            showCancelButton: false,
                            showConfirmButton: false,
                            focusConfirm: false,
                            width: 800
                        });
                    } else {
                        Swal.fire(
                            'Success',
                            'Command executed.',
                            'success'
                        );
                    }
                });
            }
        });
    }

    runCommand(command: Command) {
        return this.httpClient.post('/api/symfony-proxies/run-command', {
            command: command.command,
            directory: this.symfony.path
        });
    }

    loadSymfony() {
        if (localStorage.getItem(this.symfony.name + '_ENTRYPOINTS')) {
            const entryPoints = JSON.parse(localStorage.getItem(this.symfony.name + '_ENTRYPOINTS'));
            this.symfony.entryPoints = entryPoints ? entryPoints : [];
        }

        if (localStorage.getItem(this.symfony.name + '_COMMANDS')) {
            const commands = JSON.parse(localStorage.getItem(this.symfony.name + '_COMMANDS'));
            this.symfony.commands = commands ? commands : [];
        }

        this.symfony.favourite = this.appComponent.getFavourites().indexOf(this.symfony.name) > -1;
    }

    switchFavourite() {
        const favourites = this.appComponent.getFavourites();

        if (!this.symfony.favourite) {
            favourites.push(this.symfony.name);
            this.symfony.favourite = true;
        } else {
            favourites.splice(favourites.indexOf(this.symfony.name), 1);
            this.symfony.favourite = false;
        }

        localStorage.setItem('FAVOURITES', JSON.stringify(favourites));

        this.appComponent.sortSymfonies();
    }

    deleteSymfony() {
        Swal.fire({
            title: 'Confirm deletion?',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#dc3545',
            cancelButtonColor: '#6c757d',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                const command: Command = {
                    command: 'symfony proxy:domain:detach ' + this.symfony.name,
                    label: '',
                    priority: 0
                };
                return this.runCommand(command).subscribe(() => {
                    this.appComponent.symfonies = this.appComponent.symfonies.filter(item => {
                        return item.name !== this.symfony.name;
                    });
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }
}
