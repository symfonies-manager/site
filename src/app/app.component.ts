import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SymfonyResponse} from './utils/responses/symfony-response';
import {Symfony} from './utils/responses/symfony';
import {environment} from '../environments/environment';
import Swal from 'sweetalert2';
import {Command} from './utils/responses/command';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'symfonies-manager-site';

    symfoniesConf: SymfonyResponse = {domains: undefined, port: 0, statuses: undefined, tld: ''};
    symfonies: Symfony[] = [];
    search: string;

    constructor(private httpClient: HttpClient) {
    }

    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit() {
        this.httpClient.get<SymfonyResponse>('/api/symfony-proxies').subscribe(response => {
            this.populateSymfonies(response);
        });
    }

    populateSymfonies(response) {
        this.symfoniesConf = response;
        this.symfonies = [];

        Object.keys(response.domains).forEach(key => {
            const symfony: Symfony = {
                name: key,
                path: response.domains[key],
                url: response.statuses[key].url,
                active: response.statuses[key].isActive,
                loading: false,
                favourite: this.isFavourite(key),
                entryPoints: [],
                commands: []
            };

            this.symfonies.push(symfony);
        });

        this.sortSymfonies();
    }

    sortSymfonies() {
        this.symfonies.sort((a, b) => {
            if (a.name > b.name) {
                return 1;
            }

            if (a.name < b.name) {
                return -1;
            }

            return 0;
        }).sort((a, b) => {
            return (a.favourite ? 1 : 0) > (b.favourite ? 1 : 0) ? -1 : 0;
        });

        if (environment.autoSortStarted) {
            this.symfonies.sort((a, b) => {
                if ((a.active ? 1 : 0) < (b.active ? 1 : 0)) {
                    return 1;
                }

                if ((a.active ? 1 : 0) > (b.active ? 1 : 0)) {
                    return -1;
                }

                return 0;
            });
        }
    }

    doSearch(symfony) {
        if (!this.search) {
            return true;
        }

        return symfony.name.indexOf(this.search) > -1 || symfony.path.indexOf(this.search) > -1;
    }

    isFavourite(name) {
        return this.getFavourites().indexOf(name) > -1;
    }

    getFavourites() {
        let favourites = [];
        if (localStorage.getItem('FAVOURITES')) {
            favourites = JSON.parse(localStorage.getItem('FAVOURITES'));
        }

        return favourites;
    }

    addSymfony() {
        Swal.mixin({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2']
        }).queue([
            {
                title: 'Add symfony',
                text: 'Symfony directory path (absolute)'
            },
            {
                title: 'Add symfony',
                text: 'Symfony name'
            }
        ]).then(result => {
            if (result.value) {
                this.httpClient.post('/api/symfony-proxies/new-symfony', {
                    name: result.value[1],
                    directory: result.value[0]
                }).subscribe(symfonyUrl => {
                    const symfony: Symfony = {
                        name: result.value[1],
                        path: result.value[0],
                        active: false,
                        loading: false,
                        favourite: false,
                        url: symfonyUrl as string,
                        entryPoints: [],
                        commands: []
                    };

                    this.symfonies.push(symfony);
                    this.sortSymfonies();
                });
            }
        });
    }

}
